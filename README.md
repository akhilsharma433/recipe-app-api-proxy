# Recipe APP API proxy

NGINX proxy app for our recipe app API

# USAGE

# Environment Variables
* `LISTEN_PORT` - Post to listen on (default: `8000`)
* `AAP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_HOST` - Post of the app to forward request to (default - `9000`)